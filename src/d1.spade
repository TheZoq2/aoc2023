enum CharKind {
    Trash,
    Digit{val: int<8>},
    Newline
}

enum State {
    NoDigits,
    OneDigit{v: int<8>},
    TwoDigits{first: int<8>, last: int<8>},
}

struct StreamOut {
    val: int<8>,
    done: bool
}

pipeline(1) d1_streamer(clk: clock, rst: bool) -> StreamOut {
        let len = lib::inputs::d1p1::d1p1_length();
        reg(clk) ptr reset (rst: 0) = if ptr == len {ptr} else {trunc(ptr + 1)};
    reg;
        StreamOut$(
            val: inst(1) lib::inputs::d1p1::d1p1_input(clk, stage(-1).ptr),
            done: ptr == len,
        )
}

fn to_char_kind(char: int<8>) -> CharKind {
    if char == 10 {
        CharKind::Newline()
    } else if char >= 48 && char <= 57 {
        CharKind::Digit(trunc(char - 48))
    } else {
        CharKind::Trash()
    }
}

pipeline(2) compute_sum(clk: clock, rst: bool, s: StreamOut, converted: CharKind) -> Option<int<32>> {
        reg(clk) state reset(rst: State::NoDigits()) = {
            match (state, converted) {
                (State::NoDigits, CharKind::Digit(val)) => State::OneDigit(val),
                (State::NoDigits, _) => state,
                (State::OneDigit(first), CharKind::Digit(val)) => State::TwoDigits(first, val),
                (State::TwoDigits(first, _), CharKind::Digit(val)) => State::TwoDigits(first, val),
                (_, CharKind::Newline) => State::NoDigits(),
                (_, _) => state,
            }
        };

        let to_add: Option<int<16>> = match (state, converted) {
            (State::OneDigit(val), CharKind::Newline) => Some(val * 10 + sext(val)),
            (State::TwoDigits(first, second), CharKind::Newline) => Some(first * 10 + sext(second)),
            _ => None()
        };
    reg;
        reg(clk) sum: int<32> reset(rst: 0) = match to_add {
            Some(value) => trunc(sum + sext(value)),
            None => sum
        };
    reg;
        if s.done {
            Some(sum)
        } else {
            None()
        }
}

fn a() -> int<8> { 97u }
fn b() -> int<8> { 98u }
fn c() -> int<8> { 99u }
fn d() -> int<8> { 100u }
fn e() -> int<8> { 101u }
fn f() -> int<8> { 102u }
fn g() -> int<8> { 103u }
fn h() -> int<8> { 104u }
fn i() -> int<8> { 105u }
fn j() -> int<8> { 106u }
fn k() -> int<8> { 107u }
fn l() -> int<8> { 108u }
fn m() -> int<8> { 109u }
fn n() -> int<8> { 110u }
fn o() -> int<8> { 111u }
fn p() -> int<8> { 112u }
fn q() -> int<8> { 113u }
fn r() -> int<8> { 114u }
fn s() -> int<8> { 115u }
fn t() -> int<8> { 116u }
fn u() -> int<8> { 117u }
fn v() -> int<8> { 118u }
fn w() -> int<8> { 119u }
fn x() -> int<8> { 120u }
fn y() -> int<8> { 121u }
fn z() -> int<8> { 122u }


enum DigitState {
    Wait,
    Rest{value: int<8>, letters: [int<8>; 4], ptr: int<4>}
}

fn handle_first_char(state: DigitState, c: (int<8>, int<8>)) -> DigitState {
    if      c#0 == o() && c#1 == n() {DigitState::Rest(1, [0, 0,   0,   e()], 1)}
    else if c#0 == t() && c#1 == w() {DigitState::Rest(2, [0, 0,   0,   o()], 1)}
    else if c#0 == t() && c#1 == h() {DigitState::Rest(3, [0, r(), e(), e()], 3)}
    else if c#0 == f() && c#1 == o() {DigitState::Rest(4, [0, 0,   u(), r()], 2)}
    else if c#0 == f() && c#1 == i() {DigitState::Rest(5, [0, 0,   v(), e()], 2)}
    else if c#0 == s() && c#1 == i() {DigitState::Rest(6, [0, 0,   0,   x()], 1)}
    else if c#0 == s() && c#1 == e() {DigitState::Rest(7, [0, v(), e(), n()], 3)}
    else if c#0 == i() && c#1 == g() {DigitState::Rest(8, [0, 0,   h(), t()], 2)}
    else if c#0 == n() && c#1 == i() {DigitState::Rest(9, [0, 0,   n(), e()], 2)}
    else {DigitState::Wait()}
}

entity digit_converter(clk: clock, rst: bool, char: int<8>) -> CharKind {
    reg(clk) prev_char = char;
    reg(clk) state reset(rst: DigitState::Wait()) = match state {
        DigitState::Wait => {handle_first_char(state, (prev_char, char))},
        DigitState::Rest(out, chars, ptr) => {
            if char == chars[trunc(4 - ptr)] {
                if ptr == 1 {
                    handle_first_char(state, (prev_char, char))
                }
                else {
                    DigitState::Rest(out, chars, trunc(ptr - 1))
                }
            }
            else {
                handle_first_char(state, (prev_char, char))
            }
        }
    };

    let normal = to_char_kind(char);

    match state {
        DigitState::Rest(out, chars, ptr) => 
            if char == chars[trunc(4 - ptr)] && ptr == 1 {
                CharKind::Digit(out)
            }
            else {
                normal
            },
        _ => normal
    }
}

pipeline(5) top_d1p2(clk: clock) -> Option<int<32>> {
        reg(clk) rst_cnt: int<5> initial (5) = if rst_cnt == 0 {0} else {trunc(rst_cnt - 1)};
        let rst = !(rst_cnt == 0);

        let stream_out = inst(1) d1_streamer(clk, rst);
    reg;
    reg;
        let converted = inst digit_converter(clk, rst, stream_out.val);
    reg;
        let result = inst(2) compute_sum(clk, rst, stream_out, converted);
    reg*2;
        result
}


pipeline(5) top_d1p1(clk: clock) -> Option<int<32>> {
        reg(clk) rst_cnt: int<5> initial (5) = if rst_cnt == 0 {0} else {trunc(rst_cnt - 1)};
        let rst = !(rst_cnt == 0);

        let stream_out = inst(1) d1_streamer(clk, rst);
    reg;
    reg;
        let converted = to_char_kind(stream_out.val);
    reg;
        let result = inst(2) compute_sum(clk, rst, stream_out, converted);
    reg*2;
        result
}

