# top = d4::top_p1

#top = cocotb_sample::add_mul

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=1,
        units='ns'
    ).start())

    # wait for the reset to activate
    await FallingEdge(clk)
    await FallingEdge(clk)
    await FallingEdge(clk)
    await FallingEdge(clk)
    await FallingEdge(clk)
    await FallingEdge(clk)

    while s.o.is_eq("None()"):
        await FallingEdge(clk)

    print("Result: ", s.o.value())

    assert(False)
